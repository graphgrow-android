#define HAVE_LO

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

#include <vector>

#include "glfm.h"
#define FILE_COMPAT_ANDROID_ACTIVITY glfmAndroidGetActivity()
#include "file_compat.h"

#include "/usr/include/glm/glm.hpp"

#ifdef HAVE_LO
#include "lo/lo.h"
#endif


static PFNGLGENVERTEXARRAYSOESPROC glGenVertexArraysOES;
static PFNGLBINDVERTEXARRAYOESPROC glBindVertexArrayOES;
static PFNGLDELETEVERTEXARRAYSOESPROC glDeleteVertexArraysOES;
static PFNGLISVERTEXARRAYOESPROC glIsVertexArrayOES;
#define glGenVertexArrays glGenVertexArraysOES
#define glBindVertexArray glBindVertexArrayOES
#define glDeleteVertexArrays glDeleteVertexArraysOES
#define glIsVertexArray glIsVertexArrayOES
#define eglGetProcAddress glfmGetProcAddress

// must match audio and video implementations
// NOTE: define both to 8 when using full GUI
// otherwise breakage occurs
#define GG_AUDIO_LINKS 1
#define GG_VIDEO_LINKS 4

// ----------------------------- declarations -------------------------

struct video_control;
struct audio_control;

enum graph_mode
{
  mode_create_node,
  mode_delete_node,
  mode_move_node,
  mode_create_link,
  mode_delete_link,
  mode_flip_link,
  mode_target_link,
  mode_help
};

struct graph_node;
struct graph_link;
struct graph_arrow;
struct graph_cord;
struct graph_designer;

typedef bool (*node_mouse_t)(graph_node *, double, double);
typedef bool (*link_mouse_t)(graph_link *, double, double);
typedef bool (*arrow_mouse_t)(graph_arrow *, double, double);
typedef bool (*cord_mouse_t)(graph_cord *, double, double);
typedef bool (*designer_mouse_t)(graph_designer *, double, double);

graph_link *link_new(graph_designer *designer, graph_node *from, graph_node *to);
void link_delete(graph_link *link);
void link_leave_mode(graph_link *link);
void link_enter_mode(graph_link *link, graph_mode mode);
void link_update(graph_link *link);
graph_arrow *arrow_new();
void arrow_delete(graph_arrow *arrow);
void arrow_leave_mode(graph_arrow *arrow);
void arrow_enter_mode(graph_arrow *arrow, graph_mode mode);
graph_cord *cord_new(double x1, double y1, double x2, double y2);
void cord_delete(graph_cord *cord);
graph_node *node_new(graph_designer *designer, double x, double y, bool fixed);
void node_delete(graph_node *node);
bool nodes_linked(graph_node *a, graph_node *b);
void node_leave_mode(graph_node *node);
bool node_mouse_hit(graph_node *node, double x, double y);
bool node_mouse_down_new(graph_node *node, double x, double y);
bool node_mouse_down_delete(graph_node *node, double x, double y);
bool node_mouse_down_move(graph_node *node, double x, double y);
bool node_mouse_down_link(graph_node *node, double x, double y);
bool node_mouse_up_corded(graph_node *node, double x, double y);
void node_enter_mode(graph_node *node, graph_mode mode);
graph_designer *designer_new(int id);
void designer_leave_mode(graph_designer *designer);
void designer_enter_mode(graph_designer *designer, graph_mode mode);
void node_move_by(graph_node *node, double dx, double dy);
bool designer_mouse_down(graph_designer *designer, double x, double y);
bool designer_mouse_down_node_new(graph_designer *designer, double x, double y);
bool designer_mouse_move_dragging(graph_designer *designer, double x, double y);
bool designer_mouse_move_dragged(graph_designer *designer, double x, double y);
bool designer_mouse_move_cording(graph_designer *designer, double x, double y);
bool designer_mouse_move_corded(graph_designer *designer, double x, double y);

// ----------------------------- constants ----------------------------

const glm::vec3 white = glm::vec3(1.0f);
const glm::vec3 black = glm::vec3(0.0f);
const glm::vec3 colours[4] =
  { glm::vec3(1.000f, 0.800f, 0.000f)
  , glm::vec3(0.125f, 1.000f, 0.250f)
  , glm::vec3(0.125f, 0.533f, 1.000f)
  , glm::vec3(1.000f, 0.125f, 0.800f)
  };

static int screen_width = 1280;
static int screen_height = 800;
static int screen_border = 20;

const double minimum_x = 50;
const double minimum_y = 50;
const double maximum_x = 750;
const double maximum_y = 750;

// ----------------------------- controls -----------------------------

struct video_control
{
  int32_t active;
  int32_t count    [4];
  int32_t source   [4][GG_VIDEO_LINKS];
  float   scale    [4][GG_VIDEO_LINKS];
  float   transform[4][GG_VIDEO_LINKS][3][3];
  int32_t human;
};

struct audio_control
{
  float   scale[4][4][GG_AUDIO_LINKS];
  float   pan  [4][4][GG_AUDIO_LINKS];
  uint8_t level[4][4][GG_AUDIO_LINKS];
  uint8_t active;
};

// ----------------------------- structs ------------------------------

struct graph_link
{
  graph_designer *designer;
  graph_node *from, *to;
  double x1, y1, x2, y2;
  int target;
  bool flipped;
  graph_arrow *arrow;
  link_mouse_t on_mouse_down, on_mouse_move, on_mouse_up;
};

struct graph_arrow
{
  graph_link *link;
  double x1, y1, x2, y2, x3, y3;
  arrow_mouse_t on_mouse_down, on_mouse_move, on_mouse_up;
};

struct graph_cord
{
  double x1, y1, x2, y2;
};

struct graph_node
{
  graph_designer *designer;
  std::vector< graph_link * > link1, link2;
  bool fixed;
  double x;
  double y;
  double dragx;
  double dragy;
  double targetx;
  double targety;
  node_mouse_t on_mouse_down, on_mouse_move, on_mouse_up;
};

struct graph_designer
{
  int id;
  graph_node *start_node;
  graph_node *end_node;
  graph_node *dragging_node;
  graph_node *cording_node;
  graph_cord *cord;
  std::vector< graph_link  * >  links;
  std::vector< graph_arrow * > arrows;
  std::vector< graph_node  * >  nodes;
  designer_mouse_t on_mouse_down, on_mouse_move, on_mouse_up;
};

struct graph_draw
{
  int components;
  float tri[4096];
  GLuint vbo_tri;
  GLuint vao_tri;
  GLuint vao_quad;
  GLuint prog_circle;
  GLuint prog_arrow;
  GLuint prog_line;
};

// ----------------------------- link ---------------------------------

graph_link *link_new(graph_designer *designer, graph_node *from, graph_node *to)
{
  graph_link *link = new graph_link();
  link->designer = designer;
  link->from = from;
  link->to = to;
  link->x1 = from->x;
  link->y1 = from->y;
  link->x2 = to->x;
  link->y2 = to->y;
  link->target = designer->id;
  link->arrow = new graph_arrow();
  link->arrow->link = link;
  from->link1.push_back(link);
  to->link2.push_back(link);
  designer->links.push_back(link);
  designer->arrows.push_back(link->arrow);
  link_update(link);
  return link;
}

void link_delete(graph_link *link)
{
  for (int i = 0; i < link->from->link1.size(); )
    if (link == link->from->link1[i])
      link->from->link1.erase(link->from->link1.begin() + i);
    else
      ++i;
  for (int i = 0; i < link->to->link2.size(); )
    if (link == link->to->link2[i])
      link->to->link2.erase(link->to->link2.begin() + i);
    else
      ++i;
  for (int i = 0; i < link->designer->links.size(); )
    if (link == link->designer->links[i])
      link->designer->links.erase(link->designer->links.begin() + i);
    else
      ++i;
  for (int i = 0; i < link->designer->arrows.size(); )
    if (link->arrow == link->designer->arrows[i])
      link->designer->arrows.erase(link->designer->arrows.begin() + i);
    else
      ++i;
  arrow_delete(link->arrow);
  link->arrow = 0;
  link->from = 0;
  link->to = 0;
  link->designer = 0;
  delete link;
}

void link_leave_mode(graph_link *link)
{
  (void) link;
}

void link_enter_mode(graph_link *link, graph_mode mode)
{
  (void) link;
  (void) mode;
}

void link_update(graph_link *link)
{
  const double w = 30;
  const double c = w * -0.5;
  const double s = w * 0.8660254037844387;
  double x1 = link->x1;
  double y1 = link->y1;
  double x2 = link->x2;
  double y2 = link->y2;
  double dx = x2 - x1;
  double dy = y2 - y1;
  if (link->flipped)
  {
    dx = -dx;
    dy = -dy;
  }
  double r = sqrt(dx * dx + dy * dy);
  dx /= r;
  dy /= r;
  double ox = (x1 + x2) / 2;
  double oy = (y1 + y2) / 2;
  link->arrow->x1 = ox + w * dx;
  link->arrow->y1 = oy + w * dy;
  link->arrow->x2 = ox + c * dx + s * dy;
  link->arrow->y2 = oy - s * dx + c * dy;
  link->arrow->x3 = ox + c * dx - s * dy;
  link->arrow->y3 = oy + s * dx + c * dy;
}

// ----------------------------- arrow --------------------------------

graph_arrow *arrow_new()
{
  graph_arrow *arrow = new graph_arrow();
  arrow->on_mouse_down = 0;
  arrow->on_mouse_move = 0;
  arrow->on_mouse_up   = 0;
  return arrow;
}

void arrow_delete(graph_arrow *arrow)
{
  delete arrow;
}

void arrow_leave_mode(graph_arrow *arrow)
{
  arrow->on_mouse_down = 0;
  arrow->on_mouse_move = 0;
  arrow->on_mouse_up   = 0;
}

bool arrow_hit(graph_arrow *arrow, double x, double y)
{
  double x0 = (arrow->x1 + arrow->x2 + arrow->x3) / 3;
  double y0 = (arrow->y1 + arrow->y2 + arrow->y3) / 3;
  double dx = x - x0;
  double dy = y - y0;
  double r2 = dx * dx + dy * dy;
  return r2 < 500;
}

bool arrow_mouse_down_delete(graph_arrow *arrow, double x, double y)
{
  if (arrow_hit(arrow, x, y))
  {
    link_delete(arrow->link);
    // arrow is deleted
    return true;
  }
  return false;
}

bool arrow_mouse_down_flip(graph_arrow *arrow, double x, double y)
{
  if (arrow_hit(arrow, x, y))
  {
    arrow->link->flipped = !(arrow->link->flipped);
    link_update(arrow->link);
    return true;
  }
  return false;
}

bool arrow_mouse_down_target(graph_arrow *arrow, double x, double y)
{
  if (arrow_hit(arrow, x, y))
  {
    arrow->link->target = (arrow->link->target + 1) % 4;
    link_update(arrow->link);
    return true;
  }
  return false;
}

void arrow_enter_mode(graph_arrow *arrow, graph_mode mode)
{
  switch (mode)
  {
    case mode_delete_link:
      arrow->on_mouse_down = arrow_mouse_down_delete;
      break;
    case mode_flip_link:
      arrow->on_mouse_down = arrow_mouse_down_flip;
      break;
    case mode_target_link:
      arrow->on_mouse_down = arrow_mouse_down_target;
      break;
    case mode_create_link:
    case mode_create_node:
    case mode_delete_node:
    case mode_move_node:
    case mode_help:
      break;
  }
}

// ----------------------------- cord ---------------------------------

graph_cord *cord_new(double x1, double y1, double x2, double y2)
{
  graph_cord *cord = new graph_cord();
  cord->x1 = x1;
  cord->y1 = y1;
  cord->x2 = x2;
  cord->y2 = y2;
  return cord;
}

void cord_delete(graph_cord *cord)
{
  delete cord;
}

// ----------------------------- node ---------------------------------

graph_node *node_new(graph_designer *designer, double x, double y, bool fixed)
{
  graph_node *node = new graph_node();
  node->fixed = fixed;
  node->x = fmin(fmax(x, minimum_x), maximum_x);
  node->y = fmin(fmax(y, minimum_y), maximum_y);
  node->dragx = node->x;
  node->dragy = node->y;
  node->targetx = node->x;
  node->targety = node->y;
  node->designer = designer;
  designer->nodes.push_back(node);
  return node;
}

void node_delete(graph_node *node)
{
  graph_designer *designer = node->designer;
  for (int i = 0; i < designer->nodes.size(); )
    if (node == designer->nodes[i])
      designer->nodes.erase(designer->nodes.begin() + i);
    else
      ++i;
  while (node->link1.size())
    link_delete(node->link1[0]);
  while (node->link2.size())
    link_delete(node->link2[0]);
  node->designer = 0;
  delete node;
}

bool nodes_linked(graph_node *a, graph_node *b)
{
  for (int i = 0; i < a->link1.size(); ++i)
    if (a->link1[i]->to == b)
      return true;
  for (int i = 0; i < b->link1.size(); ++i)
    if (b->link1[i]->to == a)
      return true;
  return false;
}

void node_leave_mode(graph_node *node)
{
  node->on_mouse_down = 0;
  node->on_mouse_move = 0;
  node->on_mouse_up   = 0;
}

bool node_mouse_hit(graph_node *node, double x, double y)
{
  double dx = x - node->x;
  double dy = y - node->y;
  double r2 = dx * dx + dy * dy;
  return r2 < 1000;
}

bool node_mouse_down_new(graph_node *node, double x, double y)
{
  return node_mouse_hit(node, x, y);
}

bool node_mouse_down_delete(graph_node *node, double x, double y)
{
  if (node_mouse_hit(node, x, y))
  {
    node_delete(node);
    return true;
  }
  return false;
}

bool node_mouse_down_move(graph_node *node, double x, double y)
{
  if (node_mouse_hit(node, x, y))
  {
    node->designer->dragging_node = node;
    node->designer->on_mouse_move = designer_mouse_move_dragging;
    node->designer->on_mouse_up   = designer_mouse_move_dragged;
    node->dragx = fmin(fmax(x, minimum_x), maximum_x);
    node->dragy = fmin(fmax(y, minimum_y), maximum_y);
    return true;
  }
  return false;
}

bool node_mouse_down_link(graph_node *node, double x, double y)
{
  if (node_mouse_hit(node, x, y) && node->designer->links.size() < 8)
  {
    node->designer->cording_node = node;
    node->designer->cord = cord_new(node->x, node->y, x, y);
    for (int i = 0; i < node->designer->nodes.size(); ++i)
      if (node != node->designer->nodes[i] && ! nodes_linked(node, node->designer->nodes[i]))
        node->designer->nodes[i]->on_mouse_up = node_mouse_up_corded;
    node->designer->on_mouse_move = designer_mouse_move_cording;
    node->designer->on_mouse_up   = designer_mouse_move_corded;
    return true;
  }
  return false;
}

bool node_mouse_up_corded(graph_node *node, double x, double y)
{
  if (node_mouse_hit(node, x, y))
  {
    link_new(node->designer, node->designer->cording_node, node);
    node->designer->cording_node = 0;
    delete node->designer->cord;
    node->designer->cord = 0;
    for (int i = 0; i < node->designer->nodes.size(); ++i)
      node->designer->nodes[i]->on_mouse_up = 0;
    node->designer->on_mouse_move = 0;
    node->designer->on_mouse_up   = 0;
    return true;
  }
  return false;
}

void node_enter_mode(graph_node *node, graph_mode mode)
{
  switch (mode)
  {
    case mode_create_node:
      node->on_mouse_down = node_mouse_down_new;
      break;
    case mode_delete_node:
      if (! node->fixed)
        node->on_mouse_down = node_mouse_down_delete;
      break;
    case mode_move_node:
      if (! node->fixed)
        node->on_mouse_down = node_mouse_down_move;
      break;
    case mode_create_link:
      node->on_mouse_down = node_mouse_down_link;
      break;
    case mode_delete_link:
    case mode_flip_link:
    case mode_target_link:
    case mode_help:
      break;
  }
}

// ----------------------------- designer -----------------------------

graph_designer *designer_new(int id)
{
  graph_designer *designer = new graph_designer();
  designer->id = id;
  designer->start_node = node_new(designer, 100, 400, true);
  designer->end_node   = node_new(designer, 700, 400, true);
  designer->dragging_node = 0;
  designer->cording_node = 0;
  designer->on_mouse_down = 0;
  designer->on_mouse_move = 0;
  designer->on_mouse_up   = 0;
  return designer;
}

void designer_leave_mode(graph_designer *designer)
{
  for (int i = 0; i < designer->links.size(); ++i)
    link_leave_mode(designer->links[i]);
  for (int i = 0; i < designer->arrows.size(); ++i)
    arrow_leave_mode(designer->arrows[i]);
  for (int i = 0; i < designer->nodes.size(); ++i)
    node_leave_mode(designer->nodes[i]);
  designer->on_mouse_down = 0;
  designer->on_mouse_move = 0;
  designer->on_mouse_up   = 0;
}

void designer_enter_mode(graph_designer *designer, graph_mode mode)
{
  for (int i = 0; i < designer->links.size(); ++i)
    link_enter_mode(designer->links[i], mode);
  for (int i = 0; i < designer->arrows.size(); ++i)
    arrow_enter_mode(designer->arrows[i], mode);
  for (int i = 0; i < designer->nodes.size(); ++i)
    node_enter_mode(designer->nodes[i], mode);
  switch (mode)
  {
    case mode_create_node:
      designer->on_mouse_down = designer_mouse_down_node_new;
      break;
    case mode_delete_node:
    case mode_move_node:
    case mode_create_link:
    case mode_delete_link:
    case mode_flip_link:
    case mode_target_link:
    case mode_help:
      break;
  }
}

void node_move_by(graph_node *node, double dx, double dy)
{
  for (int i = 0; i < node->link2.size(); ++i)
  {
    node->link2[i]->x2 = fmin(fmax(node->link2[i]->x2 + dx, minimum_x), maximum_x);
    node->link2[i]->y2 = fmin(fmax(node->link2[i]->y2 + dy, minimum_y), maximum_y);
    link_update(node->link2[i]);
  }
  for (int i = 0; i < node->link1.size(); ++i)
  {
    node->link1[i]->x1 = fmin(fmax(node->link1[i]->x1 + dx, minimum_x), maximum_x);
    node->link1[i]->y1 = fmin(fmax(node->link1[i]->y1 + dy, minimum_y), maximum_y);
    link_update(node->link1[i]);
  }
  node->x = fmin(fmax(node->x + dx, minimum_x), maximum_x);
  node->y = fmin(fmax(node->y + dy, minimum_y), maximum_y);
}

bool designer_mouse_down(graph_designer *designer, double x, double y)
{
  for (int i = 0; i < designer->nodes.size(); ++i)
    if (designer->nodes[i]->on_mouse_down)
      if (designer->nodes[i]->on_mouse_down(designer->nodes[i], x, y))
        return true;
  for (int i = 0; i < designer->arrows.size(); ++i)
    if (designer->arrows[i]->on_mouse_down)
      if (designer->arrows[i]->on_mouse_down(designer->arrows[i], x, y))
        return true;
  if (designer->on_mouse_down)
    return designer->on_mouse_down(designer, x, y);
  return false;
}

bool designer_mouse_move(graph_designer *designer, double x, double y)
{
  for (int i = 0; i < designer->nodes.size(); ++i)
    if (designer->nodes[i]->on_mouse_move)
      if (designer->nodes[i]->on_mouse_move(designer->nodes[i], x, y))
        return true;
  if (designer->on_mouse_move)
    return designer->on_mouse_move(designer, x, y);
  return false;
}

bool designer_mouse_up(graph_designer *designer, double x, double y)
{
  for (int i = 0; i < designer->nodes.size(); ++i)
    if (designer->nodes[i]->on_mouse_up)
      if (designer->nodes[i]->on_mouse_up(designer->nodes[i], x, y))
        return true;
  if (designer->on_mouse_up)
    return designer->on_mouse_up(designer, x, y);
  return false;
}

bool designer_mouse_down_node_new(graph_designer *designer, double x, double y)
{
  if (minimum_x <= x && x <= maximum_x && minimum_y <= y && y <= maximum_y && designer->nodes.size() < 8)
  {
    graph_node *node = node_new(designer, x, y, false);
    node_enter_mode(node, mode_create_node);
  }
  return true;
}

bool designer_mouse_move_dragging(graph_designer *designer, double x, double y)
{
  double dx = fmin(fmax(x, minimum_x), maximum_x) - designer->dragging_node->dragx;
  double dy = fmin(fmax(y, minimum_y), maximum_y) - designer->dragging_node->dragy;
  designer->dragging_node->dragx = fmin(fmax(x, minimum_x), maximum_x);
  designer->dragging_node->dragy = fmin(fmax(y, minimum_y), maximum_y);
  node_move_by(designer->dragging_node, dx, dy);
  return true;
}

bool designer_mouse_move_dragged(graph_designer *designer, double x, double y)
{
  designer_mouse_move_dragging(designer, x, y);
  designer->on_mouse_move = 0;
  designer->on_mouse_up = 0;
  designer->dragging_node = 0;
  return true;
}

bool designer_mouse_move_cording(graph_designer *designer, double x, double y)
{
  designer->cord->x2 = fmin(fmax(x, minimum_x), maximum_x);
  designer->cord->y2 = fmin(fmax(y, minimum_y), maximum_y);
  return true;
}

bool designer_mouse_move_corded(graph_designer *designer, double x, double y)
{
  for (int i = 0; i < designer->nodes.size(); ++i)
    designer->nodes[i]->on_mouse_up = 0;
  designer->on_mouse_move = 0;
  designer->on_mouse_up = 0;
  designer->cording_node = 0;
  delete designer->cord;
  designer->cord = 0;
  return true;
  (void) x;
  (void) y;
}

// ----------------------------- drawing ------------------------------

int draw_enqueue_triangle(graph_draw *draw, int k, double x1, double y1, double x2, double y2, double x3, double y3, glm::vec3 colour)
{
  draw->tri[k++] = x1;
  draw->tri[k++] = y1;
  draw->tri[k++] = 1;
  draw->tri[k++] = 0;
  draw->tri[k++] = 0;
  draw->tri[k++] = colour[0];
  draw->tri[k++] = colour[1];
  draw->tri[k++] = colour[2];
  draw->tri[k++] = x2;
  draw->tri[k++] = y2;
  draw->tri[k++] = 0;
  draw->tri[k++] = 1;
  draw->tri[k++] = 0;
  draw->tri[k++] = colour[0];
  draw->tri[k++] = colour[1];
  draw->tri[k++] = colour[2];
  draw->tri[k++] = x3;
  draw->tri[k++] = y3;
  draw->tri[k++] = 0;
  draw->tri[k++] = 0;
  draw->tri[k++] = 1;
  draw->tri[k++] = colour[0];
  draw->tri[k++] = colour[1];
  draw->tri[k++] = colour[2];
  return k;
}


int draw_enqueue_quad(graph_draw *draw, int k, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, glm::vec3 colour)
{
  draw->tri[k++] = x1;
  draw->tri[k++] = y1;
  draw->tri[k++] = 0;
  draw->tri[k++] = 0;
  draw->tri[k++] = colour[0];
  draw->tri[k++] = colour[1];
  draw->tri[k++] = colour[2];
  draw->tri[k++] = x2;
  draw->tri[k++] = y2;
  draw->tri[k++] = 0;
  draw->tri[k++] = 1;
  draw->tri[k++] = colour[0];
  draw->tri[k++] = colour[1];
  draw->tri[k++] = colour[2];
  draw->tri[k++] = x3;
  draw->tri[k++] = y3;
  draw->tri[k++] = 1;
  draw->tri[k++] = 0;
  draw->tri[k++] = colour[0];
  draw->tri[k++] = colour[1];
  draw->tri[k++] = colour[2];
  draw->tri[k++] = x3;
  draw->tri[k++] = y3;
  draw->tri[k++] = 1;
  draw->tri[k++] = 0;
  draw->tri[k++] = colour[0];
  draw->tri[k++] = colour[1];
  draw->tri[k++] = colour[2];
  draw->tri[k++] = x2;
  draw->tri[k++] = y2;
  draw->tri[k++] = 0;
  draw->tri[k++] = 1;
  draw->tri[k++] = colour[0];
  draw->tri[k++] = colour[1];
  draw->tri[k++] = colour[2];
  draw->tri[k++] = x4;
  draw->tri[k++] = y4;
  draw->tri[k++] = 1;
  draw->tri[k++] = 1;
  draw->tri[k++] = colour[0];
  draw->tri[k++] = colour[1];
  draw->tri[k++] = colour[2];
  return k;
}


int draw_enqueue_line(graph_draw *draw, int k, double x1, double y1, double x2, double y2, double w, glm::vec3 colour)
{
  double dx = x2 - x1;
  double dy = y2 - y1;
  double r = sqrt(dx * dx + dy * dy);
  dx /= r;
  dy /= r;
  k = draw_enqueue_quad(draw, k, x1 - w * dy, y1 + w * dx, x2 - w * dy, y2 + w * dx, x1 + w * dy, y1 - w * dx, x2 + w * dy, y2 - w * dx, colour);
  return k;
}

int draw_enqueue_circle(graph_draw *draw, int k, double x, double y, double w, glm::vec3 colour)
{
  k = draw_enqueue_quad(draw, k, x - w, y - w, x + w, y - w, x - w, y + w, x + w, y + w, colour);
  return k;
}

void draw_designer(graph_draw *draw, graph_designer *designer, bool is_active, bool big)
{
  glm::vec3 bg = glm::mix(colours[designer->id], white, 0.75f);
  if (! is_active)
    bg = glm::mix(bg, black, 0.5f);
  glClearColor(bg[0], bg[1], bg[2], 0);
  glClear(GL_COLOR_BUFFER_BIT);
  int k = 0;
  for (int i = 0; i < designer->links.size() && k <= draw->components - 7 * 6; ++i)
  {
    double w = big ? 8 : 16;
    double x1 = designer->links[i]->x1;
    double y1 = designer->links[i]->y1;
    double x2 = designer->links[i]->x2;
    double y2 = designer->links[i]->y2;
    glm::vec3 colour = colours[designer->links[i]->target];
    k = draw_enqueue_line(draw, k, x1, y1, x2, y2, w, colour);
  }
  if (designer->cord)
  {
    double w = big ? 8 : 16;
    double x1 = designer->cord->x1;
    double y1 = designer->cord->y1;
    double x2 = designer->cord->x2;
    double y2 = designer->cord->y2;
    k = draw_enqueue_line(draw, k, x1, y1, x2, y2, w, colours[designer->id]);
  }
  glBufferSubData(GL_ARRAY_BUFFER, 0, k * sizeof(float), &draw->tri[0]);
  glBindVertexArray(draw->vao_quad);
  glUseProgram(draw->prog_line);
  glDrawArrays(GL_TRIANGLES, 0, k / 7);
  k = 0;
  for (int i = 0; i < designer->arrows.size() && k < draw->components - 8 * 3; ++i)
  {
    double x1 = designer->arrows[i]->x1;
    double y1 = designer->arrows[i]->y1;
    double x2 = designer->arrows[i]->x2;
    double y2 = designer->arrows[i]->y2;
    double x3 = designer->arrows[i]->x3;
    double y3 = designer->arrows[i]->y3;
    glm::vec3 colour = colours[designer->arrows[i]->link->target];
    k = draw_enqueue_triangle(draw, k, x1, y1, x2, y2, x3, y3, colour);
  }
  glBufferSubData(GL_ARRAY_BUFFER, 0, k * sizeof(float), &draw->tri[0]);
  glBindVertexArray(draw->vao_tri);
  glUseProgram(draw->prog_arrow);
  glDrawArrays(GL_TRIANGLES, 0, k / 8);
  k = 0;
  for (int i = 0; i < designer->nodes.size() && k <= draw->components - 7 * 6; ++i)
  {
    const double w = 20;
    double x = designer->nodes[i]->x;
    double y = designer->nodes[i]->y;
    glm::vec3 colour = colours[designer->nodes[i]->designer->id];
    if (designer->nodes[i]->fixed)
      colour = white;
    k = draw_enqueue_circle(draw, k, x, y, w, colour);
  }
  glBufferSubData(GL_ARRAY_BUFFER, 0, k * sizeof(float), &draw->tri[0]);
  glBindVertexArray(draw->vao_quad);
  glUseProgram(draw->prog_circle);
  glDrawArrays(GL_TRIANGLES, 0, k / 7);
}

void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *)malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    printf("program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *)malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
    }
    printf("%s shader compile info:\n%s\nshader source:\n%s", type_str, info ? info : "(no info log)", source ? source : "(no source)");
  }
  if (info) {
    free(info);
  }
}

GLuint vertex_fragment_shader(const char *vert, const char *frag) {
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(shader, 1, &vert, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_VERTEX_SHADER, vert);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(shader, 1, &frag, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_FRAGMENT_SHADER, frag);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glBindAttribLocation(program, 0, "pos");
  glBindAttribLocation(program, 1, "tcd");
  glBindAttribLocation(program, 2, "clr");
  glLinkProgram(program);
  debug_program(program);
  return program;
}

const char *draw_circle_vert =
"#version 100\n"
"precision highp float;\n"
"attribute vec2 pos;\n"
"attribute vec2 tcd;\n"
"attribute vec3 clr;\n"
"varying vec2 v_texcoord;\n"
"varying vec3 v_colour;\n"
"void main() {\n"
"  gl_Position = vec4(2.0/800.0 * pos - vec2(1.0,1.0), 0.0, 1.0);\n"
"  v_texcoord = tcd * 2.0 - vec2(1.0, 1.0);\n"
"  v_colour = clr;\n"
"}\n"
;

const char *draw_circle_frag =
"#version 100\n"
"#extension GL_OES_standard_derivatives : require\n"
"precision highp float;\n"
"varying vec2 v_texcoord;\n"
"varying vec3 v_colour;\n"
"void main() {\n"
"  float r = length(v_texcoord);\n"
"  float d = mix(length(dFdx(v_texcoord)), length(dFdy(v_texcoord)), 0.5);\n"
"  gl_FragColor = vec4(v_colour * (smoothstep(0.2, 0.2 + d, r) - smoothstep(0.8 - d, 0.8, r)), 1.0 - smoothstep(1.0 - d, 1.0, r));\n"
"}\n"
;

const char *draw_arrow_vert =
"#version 100\n"
"precision highp float;\n"
"attribute vec2 pos;\n"
"attribute vec3 tcd;\n"
"attribute vec3 clr;\n"
"varying vec3 v_texcoord;\n"
"varying vec3 v_colour;\n"
"void main() {\n"
"  gl_Position = vec4(2.0/800.0 * pos - vec2(1.0,1.0), 0.0, 1.0);\n"
"  v_texcoord = tcd;\n"
"  v_colour = clr;\n"
"}\n"
;

const char *draw_arrow_frag =
"#version 100\n"
"#extension GL_OES_standard_derivatives : require\n"
"precision highp float;\n"
"varying vec3 v_texcoord;\n"
"varying vec3 v_colour;\n"
"void main() {\n"
"  vec3 w = fwidth(v_texcoord);\n"
"  vec3 a = smoothstep(vec3(0.1), vec3(0.1) + w, v_texcoord);\n"
"  vec3 b = smoothstep(vec3(0.0), vec3(0.0) + w, v_texcoord);\n"
"  float e1 = max(min(min(a.x, a.y), a.z), 1.0 - smoothstep(0.05, 0.05 + length(w), abs(v_texcoord.y - v_texcoord.z)));\n"
"  float e2 = min(min(b.x, b.y), b.z);\n"
"  gl_FragColor = vec4(v_colour * e1, e2);\n"
"}\n"
;

const char *draw_line_vert =
"#version 100\n"
"precision highp float;\n"
"attribute vec2 pos;\n"
"attribute vec2 tcd;\n"
"attribute vec3 clr;\n"
"varying vec2 v_texcoord;\n"
"varying vec3 v_colour;\n"
"void main() {\n"
"  gl_Position = vec4(2.0/800.0 * pos - vec2(1.0,1.0), 0.0, 1.0);\n"
"  v_texcoord = tcd * 2.0 - vec2(1.0, 1.0);\n"
"  v_colour = clr;\n"
"}\n"
;

const char *draw_line_frag =
"#version 100\n"
"#extension GL_OES_standard_derivatives : require\n"
"precision highp float;\n"
"varying vec2 v_texcoord;\n"
"varying vec3 v_colour;\n"
"void main() {\n"
"  float r = abs(v_texcoord.x);\n"
"  float d = abs(dFdx(v_texcoord.x)) + abs(dFdy(v_texcoord.x));\n"
"  gl_FragColor = vec4(v_colour * (1.0 - smoothstep(0.333 - d, 0.333, r)), 1.0 - smoothstep(1.0 - d, 1.0, r));\n"
"}\n"
;

graph_draw *draw_new()
{
  graph_draw *draw = new graph_draw();
  draw->components = 4096;
  glGenBuffers(1, &draw->vbo_tri);
  glBindBuffer(GL_ARRAY_BUFFER, draw->vbo_tri);
  glBufferData(GL_ARRAY_BUFFER, draw->components * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);
  glGenVertexArrays(1, &draw->vao_tri);
  glBindVertexArray(draw->vao_tri);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 0);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((char *)0) + 2 * sizeof(GLfloat));
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), ((char *)0) + 5 * sizeof(GLfloat));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);
  glGenVertexArrays(1, &draw->vao_quad);
  glBindVertexArray(draw->vao_quad);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), 0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), ((char *)0) + 2 * sizeof(GLfloat));
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), ((char *)0) + 4 * sizeof(GLfloat));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);
  draw->prog_circle  = vertex_fragment_shader(draw_circle_vert,  draw_circle_frag);
  draw->prog_arrow   = vertex_fragment_shader(draw_arrow_vert,   draw_arrow_frag);
  draw->prog_line    = vertex_fragment_shader(draw_line_vert,    draw_line_frag);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_SCISSOR_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  return draw;
}

struct {
  graph_mode current_mode;
  int active;
  graph_designer *designer[4];
  graph_draw *draw;
  double x, y;
  pthread_t thread;
  pthread_mutex_t mutex;
  audio_control audio;
  video_control video;
  bool avdata;
  bool quit;
  time_t human;
} S;

void motioncb(double x, double y)
{
  y = screen_height - 1 - y;
  const double dx = (screen_width - screen_height) * 0.5;
  const double f = 800.0 / screen_height;
  designer_mouse_move(S.designer[S.active], (x - dx) * f, y * f);
  S.x = x;
  S.y = y;
}

void buttoncb(int action)
{
  const double button_size = 0.5 * (screen_width - screen_height) - 2 * screen_border;
    int active = -1;
    S.human = time(0);
    if (screen_border <= S.x && S.x < screen_border + button_size)
    {
      if (0.5 * (screen_height - screen_border) - button_size <= S.y && S.y < 0.5 * (screen_height - screen_border)) active = 1;
      if (0.5 * (screen_height + screen_border) <= S.y && S.y < 0.5 * (screen_height + screen_border) + button_size) active = 0;
    }
    if (screen_width - screen_border - button_size <= S.x && S.x < screen_width - screen_border)
    {
      if (0.5 * (screen_height - screen_border) - button_size <= S.y && S.y < 0.5 * (screen_height - screen_border)) active = 2;
      if (0.5 * (screen_height + screen_border) <= S.y && S.y < 0.5 * (screen_height + screen_border) + button_size) active = 3;
    }
    if (active != -1)
    {
      designer_leave_mode(S.designer[S.active]);
      S.active = active;
      designer_enter_mode(S.designer[S.active], S.current_mode);
    }
    else
    {
      const double dx = 0.5 * (screen_width - screen_height);
      const double f = 800.0 / screen_height;
      double x = S.x - dx;
      if (action == 1)
        designer_mouse_down(S.designer[S.active], x * f, S.y * f);
      if (action == -1)
        designer_mouse_up(S.designer[S.active], x * f, S.y * f);
    }
}

static bool onTouch(GLFMDisplay *display, int touch, GLFMTouchPhase phase, double x, double y) {
	motioncb(x, y);
	switch (phase)
	{
		case GLFMTouchPhaseHover:
			return false;
		case GLFMTouchPhaseBegan:
			buttoncb(1);
			return true;
		case GLFMTouchPhaseEnded:
		case GLFMTouchPhaseCancelled:
			buttoncb(-1);
		    return true;
		case GLFMTouchPhaseMoved:
			return true;
	}
}

static void *networkSendThread(void *arg)
{
	(void) arg;
    printf("Hello from thread!\n");
#ifdef HAVE_LO
	const char *video_host = "solambgel.config";
	const char *audio_host = video_host;
	lo_address at = lo_address_new(audio_host, "6060");
	lo_address vt = lo_address_new(video_host, "6061");
#endif
	do
	{
		if (0 == pthread_mutex_lock(&S.mutex))
		{
      time_t now = time(0);
			bool quit = false;
			audio_control a;
			video_control v;
			bool avdata = false;
			quit = S.quit;
			avdata = S.avdata;
			if (avdata)
			{
				memcpy(&a, &S.audio, sizeof(a));
				memcpy(&v, &S.video, sizeof(v));
				S.avdata = false;
			}
      v.human = now - S.human;
			pthread_mutex_unlock(&S.mutex);
			if (quit) break;
			if (avdata)
			{
#ifdef HAVE_LO
			    lo_blob ablob = lo_blob_new(sizeof(a), &a);
			    if (ablob)
			    {
			      if (lo_send(at, "/audio", "b", ablob) == -1)
			        printf("OSC error %d: %s\n", lo_address_errno(at), lo_address_errstr(at));
			      lo_blob_free(ablob);
			    }
			    else
			      printf("OSC error: couldn't lo_blob_new\n");
			    lo_blob vblob = lo_blob_new(sizeof(v), &v);
			    if (vblob)
			    {
			      if (lo_send(vt, "/video", "b", vblob) == -1)
			        printf("OSC error %d: %s\n", lo_address_errno(vt), lo_address_errstr(vt));
			      lo_blob_free(vblob);
			    }
			    else
			      printf("OSC error: couldn't lo_blob_new\n");
#endif
			}
		}
		usleep(1000000 / 60);
	} while (1);
	return 0;
}



static void onSurfaceCreated(GLFMDisplay *display, int width, int height) {
	screen_width = width;
	screen_height = height;
	screen_border = fmin(width, height) / 40;

    GLFMRenderingAPI api = glfmGetRenderingAPI(display);
    printf("Hello from GLFM! Using OpenGL %s\n",
           api == GLFMRenderingAPIOpenGLES32 ? "ES 3.2" :
           api == GLFMRenderingAPIOpenGLES31 ? "ES 3.1" :
           api == GLFMRenderingAPIOpenGLES3 ? "ES 3.0" : "ES 2.0");

glGenVertexArraysOES = (PFNGLGENVERTEXARRAYSOESPROC)eglGetProcAddress ( "glGenVertexArraysOES" );
glBindVertexArrayOES = (PFNGLBINDVERTEXARRAYOESPROC)eglGetProcAddress ( "glBindVertexArrayOES" );
glDeleteVertexArraysOES = (PFNGLDELETEVERTEXARRAYSOESPROC)eglGetProcAddress ( "glDeleteVertexArraysOES" );
glIsVertexArrayOES = (PFNGLISVERTEXARRAYOESPROC)eglGetProcAddress ( "glIsVertexArrayOES" );

  if (S.draw) delete S.draw;
  S.draw = draw_new();
  pthread_create(&S.thread, nullptr, networkSendThread, nullptr);
}

static void onSurfaceDestroyed(GLFMDisplay *display)
{
  pthread_join(S.thread, nullptr);
}


static void onFrame(GLFMDisplay *display, double frameTime) {

    const double button_size = 0.5 * (screen_width - screen_height) - 2 * screen_border;
    glScissor(0, 0, screen_width, screen_height);
    glViewport(0, 0, screen_width, screen_height);
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    glScissor(screen_border, 0.5 * (screen_height - screen_border) - button_size, button_size, button_size);
    glViewport(screen_border, 0.5 * (screen_height - screen_border) - button_size, button_size, button_size);
    draw_designer(S.draw, S.designer[1], 1 == S.active, false);

    glScissor(screen_border, 0.5 * (screen_height + screen_border), button_size, button_size);
    glViewport(screen_border, 0.5 * (screen_height + screen_border), button_size, button_size);
    draw_designer(S.draw, S.designer[0], 0 == S.active, false);

    glScissor(screen_width - screen_border - button_size, 0.5 * (screen_height - screen_border) - button_size, button_size, button_size);
    glViewport(screen_width - screen_border - button_size, 0.5 * (screen_height - screen_border) - button_size, button_size, button_size);
    draw_designer(S.draw, S.designer[2], 2 == S.active, false);

    glScissor(screen_width - screen_border - button_size, 0.5 * (screen_height + screen_border), button_size, button_size);
    glViewport(screen_width - screen_border - button_size, 0.5 * (screen_height + screen_border), button_size, button_size);
    draw_designer(S.draw, S.designer[3], 3 == S.active, false);

    glScissor(0.5 * (screen_width - screen_height), 0, screen_height, screen_height);
    glViewport(0.5 * (screen_width - screen_height), 0, screen_height, screen_height);
    draw_designer(S.draw, S.designer[S.active], true, true);

    GLint e;
    while ((e = glGetError()))
      printf("OpenGL ERROR %d\n", e);

    audio_control a;
    memset(&a, 0, sizeof(a));
    a.active = S.active;
    for (int i = 0; i < 4; ++i)
      for (int k = 0; k < S.designer[i]->links.size(); ++k)
      {
        graph_link *l = S.designer[i]->links[k];
        int j = l->target;
        double dx = l->x2 - l->x1;
        double dy = l->y2 - l->y1;
        double ox = l->x2 + l->x1;
        // FIXME breakage may occur here, some links might be overwritten
        a.scale[i][j][k % GG_AUDIO_LINKS] = sqrt(dx * dx + dy * dy) / 600;
        a.pan  [i][j][k % GG_AUDIO_LINKS] = (0.5 * ox - 400) / 400 * 0.5 + 0.5;
        a.level[i][j][k % GG_AUDIO_LINKS] = 1;
      }

    video_control v;
    memset(&v, 0, sizeof(v));
    v.active = S.active;
    for (int i = 0; i < 4; ++i)
    {
      v.count[i] = S.designer[i]->links.size();
      for (int j = 0; j < S.designer[i]->links.size() && j < GG_VIDEO_LINKS; ++j)
      {
        graph_link *l = S.designer[i]->links[j];
        double dx = l->x2 - l->x1;
        double dy = l->y2 - l->y1;
        if (l->flipped)
        {
          dx = -dx;
          dy = -dy;
        }
        double ox = (l->x2 + l->x1) / 2;
        double oy = (l->y2 + l->y1) / 2;
        double s  = sqrt(dx * dx + dy * dy);
        dx /= 600;
        dy /= 600;
        s  /= 600;
        ox -= 400;
        oy -= 400;
        ox /= 600;
        oy /= 600;
        oy = -oy;
        v.source[i][j] = l->target;
        v.scale [i][j] = s;
        glm::mat3 transform = glm::mat3(float(dx), float(dy), float(ox), float(-dy), float(dx), float(oy), 0.0f, 0.0f, 1.0f);
        transform = glm::inverse(transform);
        for (int p = 0; p < 3; ++p)
          for (int q = 0; q < 3; ++q)
            v.transform[i][j][p][q] = transform[p][q];
      }
    }

    if (0 == pthread_mutex_lock(&S.mutex))
    {
		memcpy(&S.audio, &a, sizeof(S.audio));
		memcpy(&S.video, &v, sizeof(S.video));
		S.avdata = true;
		pthread_mutex_unlock(&S.mutex);
	}

  time_t now = time(0);
  if (now >= S.human + 3 * 60)
  {
    for (int d = 0; d < 4; ++d)
    {
      for (auto node : S.designer[d]->nodes)
      {
        double dt = 0.0001;
        double dx = node->targetx - node->x;
        double dy = node->targety - node->y;
        node_move_by(node, dx * dt, dy * dt);
        if (! node->fixed)
        {
          double dz = dx * dx + dy * dy;
          if (dz < 25)
          {
            node->targetx = 50 + (750 - 50) * (rand() / (double) RAND_MAX);
            node->targety = 50 + (750 - 50) * (rand() / (double) RAND_MAX);
          }
        }
      }
    }
  }
}

extern void glfmMain(GLFMDisplay *display)
{
  memset(&S, 0, sizeof(S));
  srand(time(0));
  pthread_mutex_init(&S.mutex, nullptr);
  S.designer[0] = designer_new(0);
  S.designer[1] = designer_new(1);
  S.designer[2] = designer_new(2);
  S.designer[3] = designer_new(3);
  {
    auto nab = node_new(S.designer[0], 300, 400, false);
    auto nbc = node_new(S.designer[0], 400, 400 + 200 * sqrt(3)/2, false);
    auto ncd = node_new(S.designer[0], 500, 400, false);
    auto la = link_new(S.designer[0], S.designer[0]->start_node, nab);
    auto lb = link_new(S.designer[0], nab, nbc);
    auto lc = link_new(S.designer[0], nbc, ncd);
    auto ld = link_new(S.designer[0], ncd, S.designer[0]->end_node);
    la->target = 0;
    lb->target = 1;
    lc->target = 2;
    ld->target = 3;
    link_update(la);
    link_update(lb);
    link_update(lc);
    link_update(ld);
  }
  {
    double z = 600 / (1 + sqrt(2));
    auto nab = node_new(S.designer[1], 100 + z, 400, false);
    auto nbc = node_new(S.designer[1], 100 + z * (1 + sqrt(2)/2), 400 + z * sqrt(2)/2, false);
    auto ncd = node_new(S.designer[1], 100 + z, 400 + z * sqrt(2), false);
    auto la = link_new(S.designer[1], S.designer[1]->start_node, nab);
    auto lb = link_new(S.designer[1], nab, nbc);
    auto lc = link_new(S.designer[1], nbc, ncd);
    auto ld = link_new(S.designer[1], nbc, S.designer[1]->end_node);
    la->target = 1;
    lb->target = 2;
    lc->target = 3;
    ld->target = 0;
    link_update(la);
    link_update(lb);
    link_update(lc);
    link_update(ld);
  }
  {
    auto nab = node_new(S.designer[2], 400, 400, false);
    auto nbc = node_new(S.designer[2], 550, 400 - 300 * sqrt(3)/2, false);
    auto ncd = node_new(S.designer[2], 250, 400 - 300 * sqrt(3)/2, false);
    auto la = link_new(S.designer[2], S.designer[2]->start_node, nab);
    auto lb = link_new(S.designer[2], nab, nbc);
    auto lc = link_new(S.designer[2], nbc, ncd);
    auto ld = link_new(S.designer[2], nab, S.designer[2]->end_node);
    la->target = 3;
    lb->target = 1;
    lc->target = 2;
    ld->target = 0;
    link_update(la);
    link_update(lb);
    link_update(lc);
    link_update(ld);
  }
  {
    auto nab = node_new(S.designer[3], 400, 400, false);
    auto nbc = node_new(S.designer[3], 400, 700, false);
    auto ncd = node_new(S.designer[3], 400, 100, false);
    auto la = link_new(S.designer[3], S.designer[3]->start_node, nab);
    auto lb = link_new(S.designer[3], nab, nbc);
    auto lc = link_new(S.designer[3], nab, ncd);
    auto ld = link_new(S.designer[3], nab, S.designer[3]->end_node);
    la->target = 0;
    lb->target = 3;
    lc->target = 2;
    ld->target = 1;
    link_update(la);
    link_update(lb);
    link_update(lc);
    link_update(ld);
  }
  designer_enter_mode(S.designer[S.active = 0], S.current_mode = mode_move_node);

    glfmSetDisplayConfig(display,
                         GLFMRenderingAPIOpenGLES2,
                         GLFMColorFormatRGBA8888,
                         GLFMDepthFormatNone,
                         GLFMStencilFormat8,
                         GLFMMultisample4X);
    glfmSetUserData(display, &S);
	glfmSetDisplayChrome(display, GLFMUserInterfaceChromeFullscreen);
	glfmSetSurfaceCreatedFunc(display, onSurfaceCreated);
    glfmSetSurfaceResizedFunc(display, onSurfaceCreated);
    glfmSetSurfaceDestroyedFunc(display, onSurfaceDestroyed);
    glfmSetMainLoopFunc(display, onFrame);
    glfmSetTouchFunc(display, onTouch);

}
