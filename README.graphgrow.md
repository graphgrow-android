This is:

- the GLFM git repository, with an Android build system

- liblo-0.29, with a simplified CMake build system and minor patches

- graphgrow-iface.cpp from the graphgrow git repository, modified to
  work with GLFM instead of GLFW, and to send OSC to network in a non-UI
  thread
